Trackstat to iTunes Library Exporter  
  
// Licensed GPLv3 for open source use non-commerical use  
// or Spicefly Commercial License for commercial use  
//  
// https://www.spicefly.com/  
// Copyright 2019 Spicefly  
  
Purpose  
To migrate data from Trackstat XML files into iTunes Library XML format to enable importing of played date, playcount and rating into MusicBee  
  
Method  
Only builds entries that have last played date, playcount and rating metadata.  
Converts the source file location to the destination file location to enable easy importing of data.  
Format examples are; file://localhost/C:/data/Music or file:////server/music/music  
Once converted, to import into MusicBee  
Select -> File -> Library -> Import -> Import from ITunes  
Then select "overwrite rating and play counts only"  
"Add to the Inbox" as the destination for new music files not in MusicBee library  
  
Released as open source for non-commerical use, for commercial use a licence is required  
https://www.spicefly.com/  