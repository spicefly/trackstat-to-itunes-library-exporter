using System;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;
using System.Xml;

namespace TS_Tunes
{
    public partial class Form1 : Form
    {
        // Filename to output
        string file = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\spicefly_" + DateTime.Now.ToString("dd_MM_yyyy__HH_mm_ss") + ".xml";

        public Form1()
        {
            InitializeComponent();
            // try to auto set music directory
            string music = Environment.GetFolderPath(Environment.SpecialFolder.MyMusic);
            music = music.Replace("\\", "/");
            string final_music = "file://localhost/" + music;
            txt_dest.Text = final_music;
        }

        // File dialog and then process Trackstat XML file
        private void button1_Click(object sender, EventArgs e)
        {
            btn_start.Enabled = false;
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                lb_changed.Items.Clear();
                lb_changed.Items.Add(DateTime.Now + " : Starting");

                backgroundWorker1.RunWorkerAsync(openFileDialog1.FileName);     // pass filename to thread
            }
        }

        // Thread Processes
        private void backgroundWorker1_ProgessChanged(object sender, ProgressChangedEventArgs e)
        {
            string[] results = (string[])e.UserState;

            whatsup_lbl.Text = "Processed Entries:" + results[0].ToString();
            whatsup_lbl.Refresh();
        }

        private void backgroundWorker1_WorkCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btn_start.Enabled = true;
        }

        // Main Processing Thread
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            string my_file = (string)e.Argument;   // the filename parameter resurfaces here

            // Write iTunes Header
            header_write();

            int report = 0;         // Report every few tracks
            int unique_key = 0;     // Used to create unique track ID

            XmlDocument xmlDoc = new XmlDocument();     // Create an XML document object
            xmlDoc.Load(my_file);                       // Load the XML document from the specified file

            XmlNodeList xnList = xmlDoc.SelectNodes("/TrackStat/track");
            foreach (XmlNode xn in xnList)
            {
                report = report + 1;
                unique_key = unique_key + 1;
                string url = xn["url"]?.InnerText;
                string pc = xn["playCount"]?.InnerText;
                string lp = xn["lastPlayed"]?.InnerText;
                string rating = xn["rating"]?.InnerText;

                string[] my_inbound = new string[5];
                my_inbound[0] = Convert.ToString(unique_key);
                my_inbound[1] = url;
                my_inbound[2] = pc; 
                my_inbound[3] = lp;
                my_inbound[4] = rating;

                // If lp and playcount and rating are not missing process metadata
                if ( ( my_inbound[2] != null && my_inbound[3] != null && my_inbound[4] != null ))
                {
                    process_item(my_inbound);

                    if (report >= 5)            // report progress every fifth file
                    {
                        string[] workerResult = new string[1];
                        workerResult[0] = unique_key.ToString();
                        backgroundWorker1.ReportProgress(0, workerResult);
                        report = 0;
                    }
                }
            }

            // Write iTunes Footer
            footer_write();

            Action lblcopy_action = () => lb_changed.Items.Add(DateTime.Now + " : Finished");
            lb_changed.Invoke(lblcopy_action);

            Action lblcopy_action1 = () => lb_changed.Items.Add(DateTime.Now + " : All Done! Saved XML in My Documents");
            lb_changed.Invoke(lblcopy_action1);   

        }

        // Convert item and write out to XML file
        private void process_item(string[] items)
        {
            string unique_no = items[0];                        // unique key
            string ts_url = items[1]?.ToString() ?? "";         // url
            string ts_playcount = items[2]?.ToString() ?? "";   // playcount
            string ts_lastplayed = items[3]?.ToString() ?? "";  // lastplayed
            string ts_rating = items[4]?.ToString() ?? "";      // ratings in MusicBee are 0 to 5
                                                                // TODO: Trackstat could be 0 to 10

            string c_mb_url = transcode_string(ts_url);         // do transcoding of url

            // Convert Time
            string ts_x;
            if (!String.IsNullOrEmpty(ts_lastplayed))
            {
                ts_x = epoch2string(Convert.ToInt32(ts_lastplayed));
            }
            else
            {
                ts_x = "1970-01-01 00:00:00Z";
            }

            using (StreamWriter sr = File.AppendText(file))
            {

                sr.WriteLine("\t\t<key>" + unique_no + "</key>");
                sr.WriteLine("\t\t<dict>");
                if (!String.IsNullOrEmpty(ts_playcount))
                {
                    sr.WriteLine("\t\t\t<key>Play Count</key><integer>" + ts_playcount + "</integer>");
                }
                sr.WriteLine("\t\t\t<key>Location</key><string>" + c_mb_url + "</string>");
                sr.WriteLine("\t\t\t<key>Play Date UTC</key><date>" + ts_x + "</date>");
                if (!String.IsNullOrEmpty(ts_rating))
                {
                    sr.WriteLine("\t\t\t<key>Rating</key><integer>" + ts_rating + "</integer>");
                }
                sr.WriteLine("\t\t</dict>");
            }
        }

        // Transcode fileurl from TS standard to iTunes standard formatting
        private string transcode_string (string input)
        {
            // https://www.utf8-chartable.de/unicode-utf8-table.pl
            string mb_url = Uri.UnescapeDataString(input);
            mb_url = mb_url.Replace("%3A", ":");
            mb_url = mb_url.Replace("%2F", "/");
            mb_url = mb_url.Replace("%92", "%E2%80%99");

            mb_url = mb_url.Replace("%25B0", "%C2%B0");

            mb_url = mb_url.Replace("%B0", "%C2%B0");
            mb_url = mb_url.Replace("%B1", "%C2%B1");
            mb_url = mb_url.Replace("%B2", "%C2%B2");
            mb_url = mb_url.Replace("%B3", "%C2%B3");
            mb_url = mb_url.Replace("%B4", "%C2%B4");
            mb_url = mb_url.Replace("%B5", "%C2%B5");
            mb_url = mb_url.Replace("%B6", "%C2%B6");
            mb_url = mb_url.Replace("%B7", "%C2%B7");
            mb_url = mb_url.Replace("%B8", "%C2%B8");
            mb_url = mb_url.Replace("%B9", "%C2%B9");
            mb_url = mb_url.Replace("%BA", "%C2%BA");
            mb_url = mb_url.Replace("%BB", "%C2%BB");
            mb_url = mb_url.Replace("%BC", "%C2%BC");
            mb_url = mb_url.Replace("%BD", "%C2%BD");
            mb_url = mb_url.Replace("%BE", "%C2%BE");
            mb_url = mb_url.Replace("%BF", "%C2%BF");

            mb_url = mb_url.Replace("%C0", "%C3%80");
            mb_url = mb_url.Replace("%C1", "%C3%81");
            mb_url = mb_url.Replace("%C2", "%C3%82");
            mb_url = mb_url.Replace("%C3", "%C3%83");
            mb_url = mb_url.Replace("%C4", "%C3%84");
            mb_url = mb_url.Replace("%C5", "%C3%85");
            mb_url = mb_url.Replace("%C6", "%C3%86");
            mb_url = mb_url.Replace("%C7", "%C3%87");
            mb_url = mb_url.Replace("%C8", "%C3%88");
            mb_url = mb_url.Replace("%C9", "%C3%89");
            mb_url = mb_url.Replace("%CA", "%C3%8A");
            mb_url = mb_url.Replace("%CB", "%C3%8B");
            mb_url = mb_url.Replace("%CC", "%C3%8C");
            mb_url = mb_url.Replace("%CD", "%C3%8D");
            mb_url = mb_url.Replace("%CE", "%C3%8E");
            mb_url = mb_url.Replace("%CF", "%C3%8F");

            mb_url = mb_url.Replace("%D0", "%C3%90");
            mb_url = mb_url.Replace("%D1", "%C3%91");
            mb_url = mb_url.Replace("%D2", "%C3%92");
            mb_url = mb_url.Replace("%D3", "%C3%93");
            mb_url = mb_url.Replace("%D4", "%C3%94");
            mb_url = mb_url.Replace("%D5", "%C3%95");
            mb_url = mb_url.Replace("%D6", "%C3%96");
            mb_url = mb_url.Replace("%D7", "%C3%97");
            mb_url = mb_url.Replace("%D8", "%C3%98");
            mb_url = mb_url.Replace("%D9", "%C3%99");
            mb_url = mb_url.Replace("%DA", "%C3%9A");
            mb_url = mb_url.Replace("%DB", "%C3%9B");
            mb_url = mb_url.Replace("%DC", "%C3%9C");
            mb_url = mb_url.Replace("%DD", "%C3%9D");
            mb_url = mb_url.Replace("%DE", "%C3%9E");
            mb_url = mb_url.Replace("%DF", "%C3%9F");

            mb_url = mb_url.Replace("%E0", "%C3%A0");
            mb_url = mb_url.Replace("%E1", "%C3%A1");
            mb_url = mb_url.Replace("%E2", "%C3%A2");
            mb_url = mb_url.Replace("%E3", "%C3%A3");
            mb_url = mb_url.Replace("%E4", "%C3%A4");
            mb_url = mb_url.Replace("%E5", "%C3%A5");
            mb_url = mb_url.Replace("%E6", "%C3%A6");
            mb_url = mb_url.Replace("%E7", "%C3%A7");
            mb_url = mb_url.Replace("%E8", "%C3%A8");
            mb_url = mb_url.Replace("%E9", "%C3%A9");
            mb_url = mb_url.Replace("%EA", "%C3%AA");
            mb_url = mb_url.Replace("%EB", "%C3%AB");
            mb_url = mb_url.Replace("%EC", "%C3%AC");
            mb_url = mb_url.Replace("%ED", "%C3%AD");
            mb_url = mb_url.Replace("%EE", "%C3%AE");
            mb_url = mb_url.Replace("%EF", "%C3%AF");

            mb_url = mb_url.Replace("%F0", "%C3%B0");
            mb_url = mb_url.Replace("%F1", "%C3%B1");
            mb_url = mb_url.Replace("%F2", "%C3%B2");
            mb_url = mb_url.Replace("%F3", "%C3%B3");
            mb_url = mb_url.Replace("%F4", "%C3%B4");
            mb_url = mb_url.Replace("%F5", "%C3%B5");
            mb_url = mb_url.Replace("%F6", "%C3%B6");
            mb_url = mb_url.Replace("%F7", "%C3%B7");
            mb_url = mb_url.Replace("%F8", "%C3%B8");
            mb_url = mb_url.Replace("%F9", "%C3%B9");

            mb_url = mb_url.Replace("%FA", "%C3%BA");
            mb_url = mb_url.Replace("%FB", "%C3%BB");
            mb_url = mb_url.Replace("%FC", "%C3%BC");
            mb_url = mb_url.Replace("%FD", "%C3%BD");
            mb_url = mb_url.Replace("%FE", "%C3%BE");
            mb_url = mb_url.Replace("%FF", "%C3%BF");
            mb_url = mb_url.Replace("%27", "'");
            mb_url = mb_url.Replace("&", "&#38;");

            string c_mb_url = src_to_dest(mb_url);          // switch source to destination

            return c_mb_url;
        }

        // convert source filename to destination filename
        private string src_to_dest(string my_src)
        {
            string match_source = txt_source.Text;
            string match_dest = txt_dest.Text;
            
            string final_txt = my_src.Replace(match_source, match_dest);

            return final_txt;
        }

        // time convert
        private string epoch2string(int epoch)
        {
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(epoch);
            string dta = String.Format("{0:u}", dt);
            return dta;
        }

        // iTunes Header
        private void header_write()
        {

            try
            {
                using (StreamWriter sr = File.AppendText(file))
                {
                    sr.WriteLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                    sr.WriteLine("<!DOCTYPE plist PUBLIC \"-//Apple Computer//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">");
                    sr.WriteLine("<plist version=\"1.0\">");
                    sr.WriteLine("<dict>");
                    sr.WriteLine("\t<key>Major Version</key><integer>1</integer>");
                    sr.WriteLine("\t<key>Minor Version</key><integer>1</integer>");
                    sr.WriteLine("\t<key>Date</key><date>2017-05-29T20:29:37Z</date>");
                    sr.WriteLine("\t<key>Application Version</key><string>12.3.0.44</string>");
                    sr.WriteLine("\t<key>Features</key><integer>5</integer>");
                    sr.WriteLine("\t<key>Show Content Ratings</key><true/>");
                    sr.WriteLine("\t<key>Music Folder</key><string>file://localhost/C:/Music/iTunes/iTunes%20Media/</string>");
                    sr.WriteLine("\t<key>Library Persistent ID</key><string>22FA783A46B53F47</string>");
                    sr.WriteLine("\t<key>Tracks</key>");
                    sr.WriteLine("\t<dict>");
                }
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("File Write Access at Location was Denied","Access Denied",MessageBoxButtons.OK,MessageBoxIcon.Error);
                System.Environment.Exit(1);
            }
        }

        // iTunes Footer
        private void footer_write()
        {
            using (StreamWriter sr = File.AppendText(file))
            {
                sr.WriteLine("\t\t</dict>");
                sr.WriteLine("</dict>");
                sr.WriteLine("</plist>");
            }
        }

    }
}
