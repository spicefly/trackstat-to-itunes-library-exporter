namespace TS_Tunes
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btn_start = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.lb_changed = new System.Windows.Forms.ListBox();
            this.whatsup_lbl = new System.Windows.Forms.Label();
            this.txt_dest = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_source = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_start
            // 
            this.btn_start.Location = new System.Drawing.Point(12, 258);
            this.btn_start.Margin = new System.Windows.Forms.Padding(2);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(97, 28);
            this.btn_start.TabIndex = 2;
            this.btn_start.Text = "Start";
            this.btn_start.UseVisualStyleBackColor = true;
            this.btn_start.Click += new System.EventHandler(this.button1_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "TrackStat Files (*.xml)|*.xml|All files (*.*)|*.*";
            this.openFileDialog1.Title = "Select TrackStat XML file";
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgessChanged);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_WorkCompleted);
            // 
            // lb_changed
            // 
            this.lb_changed.FormattingEnabled = true;
            this.lb_changed.Location = new System.Drawing.Point(12, 49);
            this.lb_changed.Name = "lb_changed";
            this.lb_changed.Size = new System.Drawing.Size(431, 82);
            this.lb_changed.TabIndex = 4;
            // 
            // whatsup_lbl
            // 
            this.whatsup_lbl.AutoSize = true;
            this.whatsup_lbl.Location = new System.Drawing.Point(141, 266);
            this.whatsup_lbl.Name = "whatsup_lbl";
            this.whatsup_lbl.Size = new System.Drawing.Size(0, 13);
            this.whatsup_lbl.TabIndex = 5;
            // 
            // txt_dest
            // 
            this.txt_dest.Location = new System.Drawing.Point(12, 221);
            this.txt_dest.Name = "txt_dest";
            this.txt_dest.Size = new System.Drawing.Size(431, 20);
            this.txt_dest.TabIndex = 10;
            this.txt_dest.Text = "file://localhost/C:/data/Music";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 205);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(414, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Destination Music Folder- IMPORTANT MUST MATCH WHERE THE TRACKS ARE!!";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 146);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(246, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Source Music Folder As Specified in TrackStat File";
            // 
            // txt_source
            // 
            this.txt_source.Location = new System.Drawing.Point(10, 165);
            this.txt_source.Name = "txt_source";
            this.txt_source.Size = new System.Drawing.Size(433, 20);
            this.txt_source.TabIndex = 7;
            this.txt_source.Text = "file:////FATCAT/music/music";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(9, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(357, 18);
            this.label3.TabIndex = 11;
            this.label3.Text = "Files with Playcount, Rating and LastPlayed Metadata";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(13, 298);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(430, 15);
            this.label4.TabIndex = 12;
            this.label4.Text = "Released as Open Source Non-Commerical Use on Bitbucket by Spicefly.com";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(456, 323);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txt_dest);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_source);
            this.Controls.Add(this.whatsup_lbl);
            this.Controls.Add(this.lb_changed);
            this.Controls.Add(this.btn_start);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Trackstat to iTunes Library Exporter";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_start;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ListBox lb_changed;
        private System.Windows.Forms.Label whatsup_lbl;
        private System.Windows.Forms.TextBox txt_dest;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_source;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}

